#ifndef SpacePointForSeed_h
#define SpacePointForSeed_h

#include "TrkSpacePoint/SpacePoint.h"

namespace Trk{
  class SpacePoint;
}

class  SpacePointForSeed{
  public:
    SpacePointForSeed() = delete;
    SpacePointForSeed(const Trk::SpacePoint* sp) : m_spacepoint(sp) {};
    ~SpacePointForSeed() = default;
    const Trk::SpacePoint* SpacePoint() const {
      return m_spacepoint;
    }

  private:
    const Trk::SpacePoint * m_spacepoint;
};

#endif

