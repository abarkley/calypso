/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRACKERSIMEVENTTPCNV_TRACKERSIMEVENTTPCNVDICT_H
#define TRACKERSIMEVENTTPCNV_TRACKERSIMEVENTTPCNVDICT_H

//-----------------------------------------------------------------------------
//
// file:   TrackerSimEventTPCnvDict_p1.h
//
//-----------------------------------------------------------------------------


#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHitCnv_p1.h"
#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHitCollectionCnv_p1.h"
#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHitCollection_p1.h"
#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHit_p1.h"

#endif // TRACKERSIMEVENTTPCNV_TRACKERSIMEVENTTPCNVDICT_H
