# Declare the package name:
atlas_subdir(FaserActsKalmanFilter)

# External dependencies:
find_package( CLHEP )
find_package( Eigen )
find_package( Boost )
find_package( Acts COMPONENTS Core )

# Component(s) in the package:
atlas_add_library( FaserActsKalmanFilterLib
    PUBLIC_HEADERS FaserActsKalmanFilter
    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES}
    AthenaKernel
    ActsCore
    TrackerIdentifier
    TrackerReadoutGeometry
    ActsInteropLib
    FaserActsGeometryLib
    FaserActsGeometryInterfacesLib
)

atlas_add_component(FaserActsKalmanFilter
    src/*.cxx
    src/components/*.cxx
    FaserActsKalmanFilter/*.h
    FaserActsKalmanFilter/src/*.cxx
    PUBLIC_HEADERS FaserActsKalmanFilter
    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES}
    ActsCore
    EventInfo
    FaserActsKalmanFilterLib
    ActsInteropLib
    FaserActsGeometryLib
    FaserActsGeometryInterfacesLib
    MagFieldInterfaces
    MagFieldElements
    MagFieldConditions
    TrkPrepRawData
    TrackerPrepRawData
    TrackerSpacePoint
    GeoModelUtilities
    GeneratorObjects
    TrackerIdentifier
    TrackerReadoutGeometry
    Identifier
    TrackerSimData
)

# Install files from the package:
atlas_install_headers(FaserActsKalmanFilter)
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
