
# Declare the package name:
atlas_subdir( FaserActsGeometryInterfaces )

# External dependencies:
find_package( Eigen )
find_package( Acts COMPONENTS Core )

# Component(s) in the package:

atlas_add_library( FaserActsGeometryInterfacesLib
                   FaserActsGeometryInterfaces/*.h
                   INTERFACE
                   PUBLIC_HEADERS FaserActsGeometryInterfaces
                   INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS}
                   LINK_LIBRARIES ${EIGEN_LIBRARIES}
                   AthenaKernel
                   ActsInteropLib
                   ActsCore)
