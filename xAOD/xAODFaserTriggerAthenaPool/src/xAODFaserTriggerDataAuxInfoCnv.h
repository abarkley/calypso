// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef XAODFASERTRIGGERDATAATHENAPOOL_XAODFASERTRIGGERDATAAUXINFOCNV_H
#define XAODFASERTRIGGERDATAATHENAPOOL_XAODFASERTRIGGERDATAAUXINFOCNV_H

#include "xAODFaserTrigger/FaserTriggerDataAuxInfo.h"
#include "AthenaPoolCnvSvc/T_AthenaPoolAuxContainerCnv.h"

typedef T_AthenaPoolAuxContainerCnv<xAOD::FaserTriggerDataAuxInfo> xAODFaserTriggerDataAuxInfoCnv;


#endif // XAODFASERTRIGGERDATAATHENAPOOL_XAODFASERTRIGGERDATAAUXINFOCNV_H
