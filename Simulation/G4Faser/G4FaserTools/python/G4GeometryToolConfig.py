# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
from __future__ import print_function

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

GeoDetectorTool=CompFactory.GeoDetectorTool

BoxEnvelope,MaterialDescriptionTool,G4AtlasDetectorConstructionTool=CompFactory.getComps("BoxEnvelope","MaterialDescriptionTool","G4AtlasDetectorConstructionTool",)

from AthenaCommon.SystemOfUnits import mm
from VetoGeoModel.VetoGeoModelConfig import VetoGeometryCfg
from TriggerGeoModel.TriggerGeoModelConfig import TriggerGeometryCfg
from PreshowerGeoModel.PreshowerGeoModelConfig import PreshowerGeometryCfg
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg
from DipoleGeoModel.DipoleGeoModelConfig import DipoleGeometryCfg

#ToDo - finish migrating this (dnoel)
#Todo - just return component accumulator
#to still migrate: getCavernWorld, getCavernInfraGeoDetectorTool
#from ForwardRegionProperties.ForwardRegionPropertiesToolConfig import ForwardRegionPropertiesCfg

def VetoGeoDetectorToolCfg(ConfigFlags, name='Veto', **kwargs):
    #set up geometry
    result=VetoGeometryCfg(ConfigFlags)
    kwargs.setdefault("DetectorName", "Veto")
    return result, GeoDetectorTool(name, **kwargs)

def TriggerGeoDetectorToolCfg(ConfigFlags, name='Trigger', **kwargs):
    #set up geometry
    result=TriggerGeometryCfg(ConfigFlags)
    kwargs.setdefault("DetectorName", "Trigger")
    return result, GeoDetectorTool(name, **kwargs)

def PreshowerGeoDetectorToolCfg(ConfigFlags, name='Preshower', **kwargs):
    #set up geometry
    result=PreshowerGeometryCfg(ConfigFlags)
    kwargs.setdefault("DetectorName", "Preshower")
    return result, GeoDetectorTool(name, **kwargs)

def SCTGeoDetectorToolCfg(ConfigFlags, name='SCT', **kwargs):
    #set up geometry
    result=FaserSCT_GeometryCfg(ConfigFlags)
    kwargs.setdefault("DetectorName", "SCT")
    return result, GeoDetectorTool(name, **kwargs)

def DipoleGeoDetectorToolCfg(ConfigFlags, name='Dipole', **kwargs):
    #set up geometry
    result=DipoleGeometryCfg(ConfigFlags)
    kwargs.setdefault("DetectorName", "Dipole")
    return result, GeoDetectorTool(name, **kwargs)

def generateSubDetectorList(ConfigFlags):
    result = ComponentAccumulator()
    SubDetectorList=[]

    if ConfigFlags.Detector.SimulateVeto:
        accVeto, toolVeto = VetoGeoDetectorToolCfg(ConfigFlags)
        SubDetectorList += [ toolVeto ]
        result.merge(accVeto)

    if ConfigFlags.Detector.SimulateTrigger:
        accTrigger, toolTrigger = TriggerGeoDetectorToolCfg(ConfigFlags)
        SubDetectorList += [ toolTrigger ]
        result.merge(accTrigger)

    if ConfigFlags.Detector.SimulatePreshower:
        accPreshower, toolPreshower = PreshowerGeoDetectorToolCfg(ConfigFlags)
        SubDetectorList += [ toolPreshower ]
        result.merge(accPreshower)

    if ConfigFlags.Detector.SimulateFaserSCT:
        accSCT, toolSCT = SCTGeoDetectorToolCfg(ConfigFlags)
        SubDetectorList += [ toolSCT ]
        result.merge(accSCT)

    if ConfigFlags.Detector.SimulateDipole:
        accDipole, toolDipole = DipoleGeoDetectorToolCfg(ConfigFlags)
        SubDetectorList += [ toolDipole ]
        result.merge(accDipole)

    return result, SubDetectorList

def FASEREnvelopeCfg(ConfigFlags, name="Faser", **kwargs):
    result = ComponentAccumulator()

    kwargs.setdefault("DetectorName", "Faser")
    accSubDetectors, SubDetectorList = generateSubDetectorList(ConfigFlags) 
    result.merge(accSubDetectors)

    kwargs.setdefault("OffsetX", 0.0 * mm)
    kwargs.setdefault("OffsetY", 0.0 * mm)
    kwargs.setdefault("OffsetZ", 0.0 * mm)
    # kwargs.setdefault("OffsetX", 0.0 * mm)
    # kwargs.setdefault("OffsetY", 0.0 * mm)
    # kwargs.setdefault("OffsetZ", -1650.0 * mm)
    
    # kwargs.setdefault("dX", 16.0 * mm) 
    # kwargs.setdefault("dY", 16.0 * mm) 
    # kwargs.setdefault("dZ", 33.0 * mm) 
    kwargs.setdefault("dX", 600.0 * mm) 
    kwargs.setdefault("dY", 600.0 * mm) 
    kwargs.setdefault("dZ", 3000.0 * mm) 

    kwargs.setdefault("SubDetectors", SubDetectorList)

    return result, BoxEnvelope(name, **kwargs)

def G4AtlasDetectorConstructionToolCfg(ConfigFlags, name="G4FaserDetectorConstructionTool", **kwargs):
    return G4AtlasDetectorConstructionTool(name, **kwargs)

def MaterialDescriptionToolCfg(ConfigFlags, name="MaterialDescriptionTool", **kwargs):
    ## kwargs.setdefault("SomeProperty", aValue)
    return MaterialDescriptionTool(name, **kwargs)

