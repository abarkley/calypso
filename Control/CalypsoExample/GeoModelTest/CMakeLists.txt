################################################################################
# Package: GeoModelTest
################################################################################

# Declare the package name:
atlas_subdir( GeoModelTest )

# External dependencies:
find_package( GeoModelCore )

# Component(s) in the package:
atlas_add_component( GeoModelTest
                     src/GeoModelTestAlg.cxx
                     src/components/GeoModelTest_entries.cxx
                     INCLUDE_DIRS ${GEOMODELCORE_INCLUDE_DIRS}
                     LINK_LIBRARIES ${GEOMODELCORE_LIBRARIES} AthenaBaseComps GeoModelUtilities ScintReadoutGeometry TrackerReadoutGeometry MagFieldInterfaces MagFieldElements MagFieldConditions )

atlas_add_test( GeoModelCheck
                SCRIPT python ${CMAKE_CURRENT_SOURCE_DIR}/python/GeoModelTestConfig.py
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                PROPERTIES TIMEOUT 300 )

# Install files from the package:
#atlas_install_headers( GeoModelTest )
#atlas_install_joboptions( share/*.py )
atlas_install_python_modules( python/*.py )
#atlas_install_scripts( scripts/*.sh )
