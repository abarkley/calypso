#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "GeneratorObjects/McEventCollection.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"
#include "ScintSimEvent/ScintHitCollection.h" //New Sav
#include <TH1.h>


/* SimHit reading example - Ryan Rice-Smith, UC Irvine */

class SimHitAlg : public AthHistogramAlgorithm
{
    public:
    SimHitAlg(const std::string& name, ISvcLocator* pSvcLocator);

    virtual ~SimHitAlg();

    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();

    private:
    TH1* m_hist;  // Example histogram
    TH2* m_module;
    TH2* m_moduleSide1;
    TH2* m_moduleSide2;

    //ScintHit Histograms
    TH2* m_plate_preshower;
    TH2* m_plate_trigger;
    TH2* m_plate_veto;

    // Read handle keys for data containers
    // Any other event data can be accessed identically
    // Note the key names ("GEN_EVENT" or "SCT_Hits") are Gaudi properties and can be configured at run-time
    SG::ReadHandleKey<McEventCollection> m_mcEventKey       { this, "McEventCollection", "GEN_EVENT" };
    SG::ReadHandleKey<FaserSiHitCollection> m_faserSiHitKey { this, "FaserSiHitCollection", "SCT_Hits" };

    //PreshowerHits, TriggerHits, VetoHits Sav new stuff
    SG::ReadHandleKey<ScintHitCollection> m_scintHitKey { this, "ScintHitCollection", "Scint_Hits" }; //template. remove later
    SG::ReadHandleKey<ScintHitCollection> m_preshowerHitKey { this, "ScintHitCollection", "PreshowerHits" };
    SG::ReadHandleKey<ScintHitCollection> m_triggerHitKey { this, "ScintHitCollection", "TriggerHits" };
    SG::ReadHandleKey<ScintHitCollection> m_vetoHitKey { this, "ScintHitCollection", "VetoHits" };
};